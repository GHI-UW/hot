% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/riskFunctions.R
\name{ERF.acm.arem.exp}
\alias{ERF.acm.arem.exp}
\title{All-Cause Mortality, Arem, Exponential}
\usage{
ERF.acm.arem.exp(x)
}
\arguments{
\item{x}{Physical Activity (MET hrs/week)}
}
\value{
Relative Risk (Value between 0 and 1)
}
\description{
Calculates risk associated with a given amount of physical activity (MET hrs/week). Assumes exponential function shape between Arem's hazard ratio points.
}
