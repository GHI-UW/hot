context("checking getTrips")

test_that("number of trips is correct", {

    ts <- readRDS(file = system.file("example.rds", package = "HOT"))

    result <- getTrips(ts, activeTravelers = FALSE)

    expect_equal(as.numeric(result[,"walk"]), 5/15)
    expect_equal(as.numeric(result[,"cycle"]), 3/15)
    expect_equal(as.numeric(result[,"other"]), 5/15)

    result <- getTrips(ts, activeTravelers = TRUE)

    expect_equal(as.numeric(result[,"walk"]), 5/8)
    expect_equal(as.numeric(result[,"cycle"]), 3/8)
    expect_equal(as.numeric(result[,"other"]), 0/8)

})
