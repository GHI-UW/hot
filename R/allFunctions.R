#' Get local file path of stored HOT package
#'
#' This function returns the local file path where successfully-installed HOT package may be found.
#'
#' @return String file directory path for locally installed HOT package
#' @export
getHOTdirectory <- function(){
  filepath <- system.file(package = "HOT")
  
  return(filepath)
}
#' Get Risk Function for a Given Author
#'
#' This function accepts an author from related literature and returns their model (as a function) of associated Risk to activity.
#'
#' @param author Primary author name (e.g. "Lear")
#'
#' @return Risk function
#' @export
getRiskFun <- function(author = "Arem"){
  if( author == "Arem" ){ fun <- ERF.acm.arem.fun }
  else if( author == "Lear" ){ fun <- ERF.acm.lear.fun }
  else if( author == "Wen" ){ fun <- ERF.acm.wen.fun.fun }
  else{ message("Problem with author argument: ", author) }
  
  return(fun)
}
#' Estimate Total Physical Activity
#'
#' This function, called within getPAIntensity, accepts an author from related literature and returns dataframe representing multinomial distribution of total physical activity (vectors of bins, corresponding probabilities).
#'
#' @param author Primary author name (e.g. "Lear")
#' @param income If author is "Lear", this parameter constitutes a country's income level (e.g. "Upper-middle"). Otherwise, NA.
#'
#' @return Data frame of mean, standard deviation associated with author and country income level (if applicable)
#' @export
getPActivity <- function(author = "Arem",
                         income = NULL){
  
  # Conditional on author parameter, simulate a distribution of total physical activity
  if( author == "Arem" ){ P <- data.frame(bins = c(0, # DISCLAIMER :: Arem's values are for leisure activity, NOT physical activity. This is accounted for outside of this function.
                                                   7.5/2,
                                                   (7.5+15)/2,
                                                   (15+22.5)/2,
                                                   (22.5+40)/2,
                                                   (40+75)/2,
                                                   75), 
                                          probs = c(52848/661137, 
                                                    172203/661137,
                                                    170563/661137,
                                                    118169/661137,
                                                    124446/661137,
                                                    18831/661137,
                                                    4077/661137)) }
  else if( author == "Wen" ){ P <- data.frame(bins = c(3.75/2, # DISCLAIMER :: Wen's values are for leisure activity, NOT physical activity. This is accounted for outside of this function.
                                                       (3.75+7.5)/2,
                                                       (7.5+16.5)/2,
                                                       (16.5+25.5)/2,
                                                        25.5),
                                              probs = c(226493/416175,
                                                        90663/416175,
                                                        56899/416175,
                                                        21730/416175,
                                                        20390/416175)) }
  else if( author == "Lear" ){ 
    if( income == "High" ){ P <- data.frame(bins = c(10/2,
                                                     (10+50)/2,
                                                     50),
                                            probs = c(1435/13546,
                                                      4991/13546,
                                                      7120/13546)) }
    else if( income == "Upper-middle" ){ P <- data.frame(bins = c(10/2,
                                                                  (10+50)/2,
                                                                  50),
                                                         probs = c(7479/34625,
                                                                   11922/34625,
                                                                   15224/34625)) }
    else if( income == "Lower-middle" ){ P <- data.frame(bins = c(10/2,
                                                                  (10+50)/2,
                                                                  50),
                                                         probs = c(8620/53841,
                                                                   22648/53841,
                                                                   22573/53841)) }
    else if( income == "Low" ){ P <- data.frame(bins = c(10/2,
                                                         (10+50)/2,
                                                         50),
                                                probs = c(6097/28782,
                                                          9787/28782,
                                                          12898/28782)) }
    else{ message("Problem with income argument: ", income, " . P not generated.") } }
  else{ message("Problem with author argument: ", author, " . P not generated.") }

  return(P)
}
#' Estimate Intensity of Physical Activity
#'
#' This function accepts an author from related literature and returns estimated mean and standard deviation of total physical activity.
#'
#' @param author Primary author name (e.g. "Arem")
#' @param income If author is "Lear", this parameter constitutes a country's income level (e.g. "Upper-middle"). Otherwise, NA.
#'
#' @return Data frame of mean, standard deviation associated with author and country income level (if applicable)
#' @export
getPAIntensity <- function(author = "Arem",
                           income = NULL){
  
  # Calculate number of participants
  if( author == "Arem" ){ total <- 661137 }
  else if( author == "Wen" ){ total <- 416175 }
  else if( author == "Lear" ){
    if( income == "High" ){ total <- 13546 }
    else if( income == "Upper-middle" ){ total <- 34625 }
    else if( income == "Lower-middle" ){ total <- 53841 }
    else if( income == "Low" ){ total <- 28782 }
    else{ message("Problem with income argument: ", income, " . Total not set.") } }
  else{ message("Problem with author argument: ", author, " . Total not set.") }
  
  # Generate distribution of total physical activity, in the form of vectors of bins and associated probabilities
  P <- getPActivity(author,income)
  
  # Generate vector of values simulating distribution of total physical activity
  Pvec <- c()
  for (i in 1:length(P$bins)) { Pvec <- c(Pvec, rep(P$bins[i], P$probs[i]*total)) }
  
  # Return mean and sd of simulated vector distribution
  results <- data.frame(mean = mean(Pvec), sd = sd(Pvec))
  return(results)
}
