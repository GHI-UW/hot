#' Estimate Intensity from Mode Share
#'
#' Estimate the intensity from mode share, mean trip duration, and relative weights of walking and cycling
#'
#' @param modeShare Data frame of mode share by trip count: location, walk, cycle, other
#' @param meanTripDuration Data frame of mean trip duration by mode: location, walk, cycle, other
#' @param alpha Activity weights (in MET) assigned to walking and cycling. Defaults are 3 and 6, respectively.
#' @param T Average number of trips per week per person (total across all modes)
#'
#' @return Numeric value for intensity
#' @export
intensityViaModeShare <- function(modeShare, meanTripDuration, alpha = c(walk = 3, cycle = 6), T){
    
    modeShare <- modeShare[,-1]
    meanTripDuration <- meanTripDuration[,-1]
    
    alpha <- matrix(c(alpha["walk"], alpha["cycle"], 0), byrow = TRUE, ncol = 3, nrow = 1)
    intensity <- rowSums(T*modeShare*meanTripDuration*alpha/60)
    
    return(intensity)
}
#' Compute scenario modeshare
#'
#' Compute scenario modeshare from alpha, the proportional change in cycling modeshare
#'
#' @param modeShare Data frame of mode share by trip count: location, walk, cycle, other
#' @param alpha Proportional change in cycling mode share (default of 0)
#' @param beta Proportional change in walking mode share (default of 0)
#'
#' @return A data frame with mode share stratified by walk, cycle, other
#' @export
modeShareScenario <- function(modeShare, alpha = 0, beta = 0){
    
    walkCycleShare <- data.frame(walk = (1 + beta)*modeShare$walk, cycle = (1 + alpha)*modeShare$cycle)
    modeShare.scenario <- data.frame(location = modeShare$location, walkCycleShare, other = 1 - rowSums(walkCycleShare))
    
    if(any(modeShare.scenario[,-1] < 0)) message("Negative mode share.")
    if(any(modeShare.scenario[,-1] > 1)) message("Mode share greater than one.")
    
    return(modeShare.scenario)
}
#' PAF CRAFT
#'
#' CRAFT-like estimate for population attributable fraction
#'
#' @param x Numeric vector for total baseline physical activity
#' @param delta Numeric vector for change in travel activity
#' @param f Exposure risk function (ERF)
#'
#' @return Numeric vector of population attributable fractions
#' @export
PAF.CRAFT <- function(x = 12, delta = 1, f = ERF.acm.arem.exp){
    rho <- f(x)/f(x+delta)-1
    return(rho)
}
